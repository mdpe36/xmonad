#!/bin/zsh
if [[ $# != 2 ]]; then
    echo "Usage: Res.sh <device> <resolution_width>";
    exit 1;
fi

DEV=$1
WIDTH=$(xrandr | grep $DEV -A1 | tail -n1 | sed 's/ *\([0-9]*\).*$/\1/')
HEIGHT=$(xrandr | grep $DEV -A1 | tail -n1 | sed 's/.*x\([0-9]*\).*$/\1/')
CURRENT_WIDTH=$(xrandr | grep $DEV | cut -d ' ' -f 3 | sed 's/ *\([0-9]*\).*$/\1/')
CURRENT_HEIGHT=$(xrandr  | grep $DEV | cut -d ' ' -f 3 | sed 's/.*x\([0-9]*\).*$/\1/')

TARGET_WIDTH=$2
SCALE=$(($TARGET_WIDTH.0 / $WIDTH))
TARGET_HEIGHT=$(echo "$(($HEIGHT * $SCALE)))" | cut -d '.' -f1)


echo "$DEV ($WIDTH"x"$HEIGHT) from ($CURRENT_WIDTH"x"$CURRENT_HEIGHT) to ($TARGET_WIDTH"x"$TARGET_HEIGHT) with ratio $SCALE"x"$SCALE"
if [[ "$TARGET_WIDTH" == "$CURRENT_WIDTH"  && "$TARGET_HEIGHT" == "$CURRENT_HEIGHT" ]]; then
    echo "The screen is already at this resolution"
    exit 1;
elif [[ "$CURRENT_WIDTH" != "$WIDTH"  || "$CURRENT_HEIGHT" != "$HEIGHT" ]]; then
    echo "Setting 1x1 scale"

    xrandr --output $DEV --scale 1x1;
    #Necessary to work around an issue where re-scaling
    #only works if the scale is set to 1x1
    sleep 3;
fi

echo "Setting new scale"
arg="$SCALE"x"$SCALE"
xrandr --output $DEV --scale $arg
